﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace appaddbordersandshapes
{
    public enum WdLineStyle
    {
        wdLineStyleNone,
        wdLineStyleSingle,
        wdLineStyleDot,
        wdLineStyleDashSmallGap,
        wdLineStyleDashLargeGap,
        wdLineStyleDashDot,
        wdLineStyleDashDotDot,
        wdLineStyleDouble,
        wdLineStyleTriple,
        wdLineStyleThinThickSmallGap,
        wdLineStyleThickThinSmallGap,
        wdLineStyleThinThickThinSmallGap
    }
}
