﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace appaddbordersandshapes
{
    public enum WdBorderType
    {
        wdBorderTop = -1,
        wdBorderLeft = -2,
        wdBorderBottom = -3,
        wdBorderRight = -4,
        wdBorderHorizontal = -5,
        wdBorderVertical = -6,
        wdBorderDiagonalDown = -7,
        wdBorderDiagonalUp = -9
    }
}
