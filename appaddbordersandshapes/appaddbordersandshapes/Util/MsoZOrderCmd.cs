﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace appaddbordersandshapes
{
    public enum MsoZOrderCmd
    {
        msoBringToFront,
        msoBringForward,
        msoSendBackward,
        msoBringInFrontOfText,
        msoSendBehindText,
        msoBehindAll
    }
}
