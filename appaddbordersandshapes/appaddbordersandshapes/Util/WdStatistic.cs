﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace appaddbordersandshapes
{
    /// <summary>
    /// Specifies a statistic to return from a selection or item.
    /// </summary>
    public enum WdStatistic
    {
        wdStatisticWords,
        wdStatisticLines,
        wdStatisticPages,
        wdStatisticCharacters,
        wdStatisticParagraphs,
        wdStatisticCharactersWithSpaces,
        wdStatisticFarEastCharacters
    }
}
