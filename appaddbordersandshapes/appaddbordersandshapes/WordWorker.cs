﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace appaddbordersandshapes
{
    public class WordWorker
    {
        private dynamic word = null;
        private readonly string appProgID = "Word.Application";
        private string path;
        private dynamic wordDoc;
        private int documentPageCount = 0;
        private dynamic select = null;

        public WordWorker(string path)
        {
            this.path = path;
            //Получаем экземпляр Word
            Type wordType = Type.GetTypeFromProgID(appProgID);
            word = Activator.CreateInstance(wordType);

            if (word == null)
                throw new Exception("Отсутствует подключение к Word");

            try
            {   //Открытие документа
                wordDoc = word.Documents.Open(path);
                select = word.Selection.Sections[1];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        /// <summary>
        /// Добавляет в документ границы и подложку
        /// </summary>
        public void AddBordersAndShapes()
        {
            try
            {
                SetBorders();

                for (int i = 1; i <= documentPageCount + 1; i++)
                {
                    SetColorShape(select);
                    word.Selection.GoTo(1, 1, documentPageCount, i);
                }
            }catch
            {
                CloseWordApp(MsoWordTypeBoolean.msoFalse);
            }
        }

        public void SetWordVisible()
        {
            if (word == null)
                return;

            word.Visible = true;
        }

        public bool CloseWordApp(MsoWordTypeBoolean canSaveChange)
        {
            bool isSuccess = false;
            if (word == null)
                return !isSuccess;

            word.Quit(canSaveChange);
            word = null;

            return !isSuccess;
        }

        /// <summary>
        /// Устанавливает вид и размеры границ
        /// </summary>
        private void SetBorders()
        {
            documentPageCount = GetPagesCount();

            WdBorderType[] borders = new WdBorderType[] {WdBorderType.wdBorderLeft,
                                                         WdBorderType.wdBorderRight,
                                                         WdBorderType.wdBorderTop,
                                                         WdBorderType.wdBorderBottom};

            foreach (var border in borders)
            {
                select.Borders[border].LineStyle = WdLineStyle.wdLineStyleThinThickThinSmallGap;
                select.Borders[border].LineWidth = OutsideLineWidth.wdLineWidth300pt;
                select.Borders[border].Color = -687800321;
            }
        }

        /// <summary>
        /// Добавляет автофигуру заданного формата
        /// </summary>
        /// <param name="Select"></param>
        private void SetColorShape(dynamic Select)
        {
            float top = 28.5F;
            float left = 27F;
            float height = 784.35F;
            float width = 541.7F;

            wordDoc.Shapes.AddShape(MsoAutoShapeType.msoShapeRectangle, left, top, width, height);
            Select.Range().ShapeRange.Line.Visible = MsoWordTypeBoolean.msoFalse;
            Select.Range().ShapeRange.Fill.ForeColor.RGB = Color.FromArgb(255, 244, 221);
            Select.Range().ShapeRange.Fill.Visible = MsoWordTypeBoolean.msoTrue;
            Select.Range().ShapeRange.LockAnchor = MsoWordTypeBoolean.msoTrue;
            Select.Range().ShapeRange.ZOrder(MsoZOrderCmd.msoBehindAll);
        }

        private int GetPagesCount()
        {
            return wordDoc.ComputeStatistics(WdStatistic.wdStatisticPages, false);
        }
    }
}
