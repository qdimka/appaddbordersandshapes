﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace appaddbordersandshapes
{
    public partial class Form1 : Form
    {
        private WordWorker wordWorker;
        private OpenFileDialog dialog;

        public Form1()
        {
            InitializeComponent();
            dialog = new OpenFileDialog();
            dialog.Title = "Выберите документ";
            dialog.Filter = "Документ RTF|*.rtf";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            wordWorker = new WordWorker(dialog.FileName);
            wordWorker.AddBordersAndShapes();

            if (!checkBox1.Checked)
            {
                wordWorker.CloseWordApp(MsoWordTypeBoolean.msoTrue);
            }
            else
            {
                wordWorker.SetWordVisible();
            }
        }
    }
}
